package com.logicalthinking.coder.controller;

import com.logicalthinking.coder.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("redis")
public class RedisController {

    String SUCCESS = "success";

    @Autowired
    private RedisUtil redisUtil;


    @ResponseBody
    @RequestMapping("addKey")
    public String setKey(String key, String value) {
        redisUtil.set(key, value);
        return SUCCESS;
    }

    @ResponseBody
    @RequestMapping("getKey")
    public String setKey(String key) {
        return (String) redisUtil.get(key);
    }
}
