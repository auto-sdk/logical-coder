package com.logicalthinking.coder.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 控制器
 *
 * @author lanping
 * @version 1.0
 * @date 2019-06-29
 */
@Controller
public class ViewController {

    /**
     * index
     */
    @RequestMapping(value = "/index")
    public String index(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("sessionId:" + request.getSession().getId());
        return "index";
    }
}