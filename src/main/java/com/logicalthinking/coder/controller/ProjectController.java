package com.logicalthinking.coder.controller;

import com.logicalthinking.coder.utils.ConstantUtil;
import com.logicalthinking.coder.utils.ResultEntity;
import com.lp.auto.sdk.manager.CreateProjectManager;
import com.lp.auto.sdk.manager.ProjectManager;
import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.utils.Constant;
import com.lp.auto.sdk.utils.PropertiesUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-06-30
 **/
@Controller
@RequestMapping("manage")
public class ProjectController {

    private static Logger logger = LoggerFactory.getLogger(ProjectController.class);

    /**
     * 创建代码
     */
    @ResponseBody
    @RequestMapping(value = "/createCoder", method = RequestMethod.POST)
    public ResultEntity<Object> createCoder(Connector connector, ProjInfo projectInfo,
                                            HttpServletRequest request, HttpServletResponse response) {
        try {
            ProjectManager projectManager = CreateProjectManager.getProjectManager(connector.getDbType());
            if (projectManager != null) {
                projectManager.createFiles(connector, projectInfo);
                return new ResultEntity<Object>(ConstantUtil.CODE_200, "创建成功");
            } else {
                return new ResultEntity<Object>(ConstantUtil.CODE_404, "数据库类型错误！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultEntity<Object>(ConstantUtil.CODE_500, e.getMessage());
        }
    }

    /**
     * 创建代码
     */
    @ResponseBody
    @RequestMapping(value = "/createFilesByOneTable", method = RequestMethod.POST)
    public ResultEntity<Object> createFilesByOneTable(Connector connector, ProjInfo projectInfo, EntityInfo entityInfo,
                                                      HttpServletRequest request, HttpServletResponse response) {
        try {
            ProjectManager projectManager = CreateProjectManager.getProjectManager(connector.getDbType());
            if (projectManager != null) {
                projectManager.createFilesByOneTable(connector, projectInfo, entityInfo);
                return new ResultEntity<Object>(ConstantUtil.CODE_200, "创建成功");
            } else {
                return new ResultEntity<Object>(ConstantUtil.CODE_404, "数据库类型错误！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultEntity<Object>(ConstantUtil.CODE_500, e.getMessage());
        }
    }

    /**
     * 下载文件
     */
    @RequestMapping(value = "/downloadFile")
    public void downloadFile(HttpServletRequest request, HttpServletResponse response) {
        String fileName = request.getParameter("fileName");
        if (StringUtils.isNotBlank(fileName)) {
            try {
                fileName = URLDecoder.decode(fileName, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
            File file = new File(rootDir + fileName);
            if (file.exists()) {
                try {
                    InputStream fis = new BufferedInputStream(
                            new FileInputStream(file));
                    byte[] buffer = new byte[fis.available()];
                    fis.read(buffer);
                    fis.close();
                    response.reset();
                    // 先去掉文件名称中的空格,然后转换编码格式为utf-8,保证不出现乱码,这个文件名称用于浏览器的下载框中自动显示的文件名
                    response.addHeader(
                            "Content-Disposition",
                            "attachment;filename="
                                    + new String(fileName.getBytes("utf-8"),
                                    "iso8859-1"));
                    response.addHeader("Content-Length", "" + file.length());
                    OutputStream os = new BufferedOutputStream(
                            response.getOutputStream());
                    response.setContentType("application/octet-stream");
                    os.write(buffer);// 输出文件
                    os.flush();
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                file.delete();
            } else {
                logger.info("file {} not found ", fileName);
            }
        } else {

        }
    }
}
