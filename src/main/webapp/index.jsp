<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<%= basePath%>">
    <meta charset="UTF-8">
    <title>首页</title>
    <style>
        .a {
            color: #36C;
            text-decoration: none;
        }
    </style>
</head>
<body>
<h1>欢迎您</h1>
<h2>
    <a href="index" class="a">创建项目</a>
</h2>
</body>
</html>