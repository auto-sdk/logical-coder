<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<%= basePath%>">
    <meta charset="UTF-8">
    <title>创建项目</title>
    <script type="application/javascript" src="style/js/jquery-2.0.3.min.js"></script>
    <script type="application/javascript" src="style/js/jquery.validate.js"></script>
    <style>
        * {
            margin: 0 auto;
            padding: 0;
        }

        body {
            width: 800px;
            margin: 0 auto;
            padding: 0;
            font-family: 微软雅黑;
        }

        .header-title {
            margin-top: 20px;
        }

        .form {
            width: 100%;
            height: auto;
            margin-top: 20px;
        }

        .form-title {
            font-size: 16px;
            font-weight: bold;
            line-height: 40px;
            height: 40px;
        }

        .form-content {
            width: 100%;
        }

        .form-content .form-line {
            width: 100%;
            height: 50px;
            line-height: 50px;
            font-size: 14px;
        }

        .form-content .form-line label:first-child {
            width: 23%;
            float: left;
        }

        .form-content .form-line input[type='text'] {
            width: 75%;
            float: left;
            border-radius: 2px;
            border: 1px solid #DDD;
            height: 34px;
            margin-top: 8px;
            padding-left: 5px;
            font-size: 14px;
        }

        .form-content .form-line select {
            width: 76%;
            float: left;
            height: 34px;
            border-radius: 2px;
            border: 1px solid #DDD;
            margin-top: 8px;
            padding-left: 5px;
            font-size: 14px;
        }

        .form-btn {
            height: 60px;
            line-height: 60px;
            text-align: center;
        }

        .btn {
            padding: 12px 25px;
            border: none;
            background: #36C;
            color: #FFF;
            cursor: pointer;
            border-radius: 3px;
            outline: none;
            opacity: 0.8;
        }

        .btn:hover {
            opacity: 1;
        }

        .choose-line {
            float: left;
            width: 75%;
        }

        .tooltip {
            position: absolute;
            z-index: 1020;
            display: block;
            visibility: visible;
            padding: 5px;
            font-size: 11px;
            opacity: 0;
            filter: alpha(opacity=0);
        }

        .tooltip-inner {
            max-width: 200px;
            padding: 3px 8px;
            color: #ffffff;
            text-align: center;
            text-decoration: none;
            background-color: #000000;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        form input.error {
            color: #F00;
            border: 1px solid #CE7979;
            background: #FFF7FA;
        }

        form label.error {
            color: #F00;
        }

    </style>
</head>
<body>
<h1 class="header-title">创建项目</h1>
<form id="projectForm" class="form" name="projectForm"
      action="" method="post">
    <div class="form-title">项目信息</div>
    <div class="form-content">
        <div class="form-line">
            <label>类型</label>
            <div class="choose-line">
                <label style="width: auto;">
                    <input type="radio" value="full" validate="required:true" name="projType" checked="checked"/> 完整项目
                </label>
                <label style="width: auto;margin-left: 20px;">
                    <input type="radio" value="single" validate="required:true" name="projType"/> 单表
                </label>
            </div>
        </div>
        <div class="form-line">
            <label>项目名称</label>
            <input type="text" name="projectName" validate="required:true" id="projectName" value="autoCoder">
        </div>
        <div class="form-line">
            <label>项目包名</label>
            <input type="text" name="packageName" validate="required:true" id="packageName" value="com.logicalthinking">
        </div>
        <div class="form-line">
            <label>开发者名称</label>
            <input type="text" name="author" validate="required:true" id="author" value="lanping">
        </div>
    </div>
    <div class="form-title">数据库连接信息</div>
    <div class="form-content">
        <div class="form-line">
            <label>数据库类型</label>
            <select id="dbType" name="dbType" validate="required:true" onchange="changeDB(this.value)">
                <%--<option value="sqlserver">SQL Server</option>
                <option value="mysql" selected>MySQL</option>
                <option value="db2">DB2</option>--%>
            </select>
        </div>
        <div class="form-line">
            <label>数据库驱动</label>
            <input type="text" name="driver" validate="required:true" id="driver" defaultValue="com.mysql.jdbc.Driver">
        </div>
        <div class="form-line">
            <label>连接地址</label>
            <input type="text" name="url" id="url" validate="required:true"
                   defaultValue="jdbc:mysql://127.0.0.1:3306/store?useUnicode=true&characterEncoding=utf-8">
        </div>
        <div class="form-line">
            <label>用户名</label>
            <input type="text" name="user" id="user" validate="required:true" defaultValue="root">
        </div>
        <div class="form-line">
            <label>密码</label>
            <input type="text" name="password" validate="required:true" id="password" defaultValue="root123456">
        </div>
        <div class="form-line" id="db2Schema" style="display: none;">
            <label>数据库模式</label>
            <input type="text" name="schema" validate="required:true" id="schema" defaultValue="test">
        </div>
        <div class="form-line singleTable" id="tablename-line" style="display: none;">
            <label>表名</label>
            <input type="text" name="tableName" validate="required:true" id="tableName" defaultValue="users">
        </div>
        <div class="form-line singleTable" validate="required:true" id="tableComment-line" style="display: none;">
            <label>表注释</label>
            <input type="text" name="tableComment" id="tableComment" defaultValue="用户信息表">
        </div>
    </div>
    <div class="form-btn">
        <input type="button" id="submit" class="btn" value="创建项目">
        <input type="button" id="singleSubmit" style="margin-left: 20px;display: none;"
               class="btn" value="创建单表类">
    </div>
</form>
<script>

    var dbData = {
        dbType: [
            {
                label: "SQL Server",
                code: "sqlserver"
            },
            {
                label: "MySQL",
                code: "mysql"
            },
            {
                label: "DB2",
                code: "db2"
            }
        ],
        dbConn: [
            {
                dbType: "mysql",
                connector: {
                    driver: "com.mysql.jdbc.Driver",
                    url: "jdbc:mysql://127.0.0.1:3306/store?useUnicode=true&characterEncoding=utf-8",
                    user: "root",
                    password: "root123456"
                }
            },
            {
                dbType: "sqlserver",
                connector: {
                    driver: "com.microsoft.sqlserver.jdbc.SQLServerDriver",
                    url: "jdbc:sqlserver://127.0.0.1:1433;databaseName=base_db",
                    user: "sa",
                    password: "123456"
                }
            },
            {
                dbType: "db2",
                connector: {
                    driver: "com.ibm.db2.jcc.DB2Driver",
                    url: "jdbc:db2://127.0.0.1:50000/base_db",
                    user: "db2admin",
                    password: "123456",
                    schema: "db2admin"
                }
            }
        ]
    };

    var valid;
    $(function () {
        var defaultDB = "mysql";
        var dbTypes = '';
        for (var i = 0; i < dbData.dbType.length; i++) {
            var select = '';
            if (dbData.dbType[i].code == defaultDB) {
                select = ' selected="selected"';
            }
            dbTypes += '<option value="' + dbData.dbType[i].code + '"' + select + '>' + dbData.dbType[i].label + '</option>';
        }
        $("#dbType").html(dbTypes);

        changeDB(defaultDB);

        $("input[name='projType']").click(function () {
            if (this.value == "single") {
                $(".singleTable").show();
                $("#singleSubmit").show();
                $("#submit").hide();
            } else if (this.value == "full") {
                $(".singleTable").hide();
                $("#singleSubmit").hide();
                $("#submit").show();
            }
        });

        valid = $("#projectForm").validate({
            rules: {
                projectName: {
                    required: true
                }
            },
            messages: {
                projectName: {
                    required: "请输入项目名称！"
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            invalidHandler: function (form, validator) {
                return false;
            },
            errorElement: 'div',
            errorPlacement: function (error, element) {
                error.addClass('tooltip tooltip-inner');
                element.after(error);
                var pos = $.extend({}, element.offset(), {
                        width: element.outerWidth()
                        , height: element.outerHeight()
                    }),
                    actualWidth = error.outerWidth(),
                    actualHeight = error.outerHeight();
                error.css({
                    display: 'block',
                    opacity: '0.8',
                    top: pos.top - actualHeight,
                    left: pos.left + pos.width / 2 - actualWidth / 2
                });
            },
            highlight: function (element, errorClass) {
                //高亮显示
                $(element).addClass(errorClass);
                $(element).parents('li:first').children('label').addClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
                $(element).parents('li:first').children('label').removeClass(errorClass);
            }
        })
    });

    function changeDB(val) {
        for (var i = 0; i < dbData.dbConn.length; i++) {
            if (val == dbData.dbConn[i].dbType) {
                var conn = dbData.dbConn[i].connector;
                $("#driver").val(conn.driver);
                $("#url").val(conn.url);
                $("#user").val(conn.user);
                $("#password").val(conn.password);
                $("#schema").val(conn.schema);
                if (val == "db2") {
                    $("#db2Schema").show();
                } else {
                    $("#db2Schema").hide();
                }
            }
        }
    }

    $("#submit").click(function () {
        if (!valid.form()) {
            return;
        }
        ;
        $.ajax({
            url: "manage/createCoder",
            type: "post",
            data: $("#projectForm").serialize(),
            dataType: "json",
            success: function (res) {
                if (res.error_code == "200") {
                    //alert(res.msg);
                    var projectName = $("#projectName").val();
                    location.href = "manage/downloadFile?fileName=" +
                        encodeURIComponent(encodeURIComponent(projectName)) + ".zip";
                } else {
                    alert(res.msg);
                }
            }, error: function (data) {
                alert("创建失败！");
            }
        });
    });

    $("#singleSubmit").click(function () {
        $.ajax({
            url: "manage/createFilesByOneTable",
            type: "post",
            data: $("#projectForm").serialize(),
            dataType: "json",
            success: function (res) {
                if (res.error_code == "200") {
                    //alert(res.msg);
                    var projectName = $("#projectName").val();
                    location.href = "manage/downloadFile?fileName=" +
                        encodeURIComponent(encodeURIComponent(projectName)) + ".zip";
                } else {
                    alert(res.msg);
                }
            }, error: function (data) {
                alert("创建失败！");
            }
        });
    });

</script>
</body>
</html>